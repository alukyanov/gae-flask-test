from google.appengine.ext import ndb, db
from google.appengine.api import users
import flask
from flask import Flask, request, abort, render_template, url_for
app = Flask(__name__)

from models import Message
from decorators import JsonResponse


@app.before_request
def before_request():
    method = request.form.get('_method', '').upper()
    if method:
        request.environ['REQUEST_METHOD'] = method
        ctx = flask._request_ctx_stack.top
        ctx.url_adapter.default_method = method
        assert request.method == method


@app.route('/')
def index_page():
    return render_template('index_page.html')


@app.route('/admin/message')
def message_page():
    user = users.get_current_user()
    if user is not None:
        print dir(user)
        objects_list = Message.query().fetch(10)
        for obj in objects_list:
            obj.id = obj.key.id()
        return render_template(
            'message_form.html', 
            objects_list=objects_list,
            logout_url=users.create_logout_url('/'))
    login_url = users.create_login_url(url_for('message_page'))
    return 'access denied. please <a href="%s">Sign in</a>' % login_url


@app.route('/admin')
def admin():
    user = users.get_current_user()
    if user:
        return 'access granted'
    elif user.is_current_user_admin():
        return 'admin access granted'
    else:
        login_url = users.create_login_url(dest_url='localhost:8080')
        return 'access denied: <a href="%s">Sign in</a>' % login_url
    return 'admin page'


@app.route('/api/message', methods=['POST', 'PUT'])
@app.route('/api/message/<oid>', methods=['POST', 'PUT'])
@JsonResponse
def api_message(oid=None):
    if request.method == 'POST':
        if 'message' in request.form:
            msg = Message(message=request.form['message'])
            oid = msg.put()
            return 'message saved successfully [%s]' % oid
        else:
            raise Exception('Form error')
    elif request.method == 'PUT':
        if 'message' not in request.form:
            raise Exception('Form error')
        if oid is None and not request.form.get('oid'):
            raise Exception('Message ID not defined')
        if oid is None:
            oid = request.form['oid']
        msg = Message.update_message(oid, request.form['message'])
        if msg is None:
            raise Exception('message with id [%s] not found' % oid)
        return 'Message updated successfully'
    else:
        raise Exception('HTTP POST and PUT methods required')
    raise Exception('something went wrong')
