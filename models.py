from google.appengine.ext import ndb

from validators import validate_message


class Message(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    modified = ndb.DateTimeProperty(auto_now=True)
    message = ndb.TextProperty(required=True, validator=validate_message)

    @classmethod
    @ndb.transactional(retries=1)
    def update_message(cls, oid, message):
        msg = ndb.Key(Message, int(oid)).get()
        msg.message = message
        msg.put()
        return msg
