import simplejson
from functools import wraps
from flask import make_response


def JsonResponse(m):
    @wraps(m)
    def _call_(**kwargs):
        data = {}
        try:
            result = m(**kwargs)
            data['success'] = result
        except Exception, e:
            data['error'] = 'An exception was occured: %s' % e
        resp = make_response(simplejson.dumps(data))
        resp.headers['Content-Type'] = 'application/json charset=UTF-8'
        return resp
    return _call_

